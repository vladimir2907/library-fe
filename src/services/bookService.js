import { url } from "../constants/common";

export const getAllBooks = async (token, query) => {
    const response = await fetch(`${url}/api/book`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    });

    const data = await response.json();

    return data;
}

export const getOneBook = async (id, token) => {
    const response = await fetch(`${url}/api/book/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    });

    const data = await response.json();

    return data;
}

export const searchBooks = async (token, search) => {
    const response = await fetch(`${url}/api/book/search`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({pattern: search})

    });

    const data = await response.json();

    return data;
}