import { url } from '../constants/common';

export const register = async (authData) => {
    const response = await fetch(`${url}/api/user/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(authData)
    });

    const data = await response.json();

    return data;
}

export const login = async (authData) => {
    const response = await fetch(`${url}/api/user/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(authData)
    })

    const data = await response.json();

    return data;
}

export const logout = async (token) => {
    const response = await fetch(`${url}/api/user/logout`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
            'Access-Control-Allow-Origin': 'no-cors'
        }
    })

    const data = await response.json();

    return data;
}