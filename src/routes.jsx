import { Navigate } from 'react-router-dom';

import Login from './components/Login/Login';
import Register from './components/Register/Register';
import Home from './components/Home/Home';
import Details from './components/Details/Details';
import Settings from './components/Settings/Settings';

const routeConfig = (isLoggedIn) => [
    {
        path: '/',
        element: isLoggedIn ? <Home /> : <Navigate to='/login' />,
    },
    {
        path: '/register',
        element: !isLoggedIn ? <Register /> : <Navigate to='/' />,
    },
    {
        path: '/login',
        element: !isLoggedIn ? <Login /> : <Navigate to='/' />,
    },
    {
        path: '/book/:bookId',
        element: isLoggedIn ? <Details /> : <Navigate to='/login' />,
    },
    {
        path: '/settings',
        element: isLoggedIn ? <Settings /> : <Navigate to='/login' />,
    },
];

export default routeConfig;