import './Settings.css';
import Navbar from '../Navbar/Navbar';

const Settings = () => {
    return (
        <>
            <Navbar />
            <div id='settings-wrapper'>
                <div className='settings-section'>
                    <div className='settings-type'>
                        <h2>GENERAL SETTINGS</h2>
                    </div>
                    <div className='option'>
                        <div><p>NOTIFICATIONS AND EMAILS</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>

                    </div>
                    <div className='option'>
                        <div><p>USER MANAGEMENT</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>

                    </div>
                    <div className='option'>
                        <div><p>PHYSICAL LIBRARIES</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>READING EVENTS</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>INVOICING</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>READERS STATISTICS</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>EVENTS STATISTICS</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='settings-section'>
                    <div id='book-settings' className='settings-type'>
                        <div>
                            <h2>BOOK SETTINGS</h2>
                        </div>
                        <div id='add-new'>
                            <button>ADD NEW</button>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>MANAGE GENRES</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>BOOK VISIBILITY</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>AUTHORS DATABASE</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className='option'>
                        <div><p>BOOK COVERS</p></div>
                        <div className='line'>
                            <div className='arrow-container'>
                                <img src={window.location.origin + '/images/arrow.svg'} alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default Settings;