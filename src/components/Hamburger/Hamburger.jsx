import './Hamburger.css';
import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import { logout } from '../../services/authService';

const Hamburger = () => {
    const [toggleMenu, setToggleMenu] = useState(false);
    const [isLibraryVisible, setIsLibraryVisible] = useState(false);
    const [isSettingsVisible, setIsSettingsVisible] = useState(false);
    const navigate = useNavigate();
    const { pathname } = useLocation();
    const { token } = JSON.parse(localStorage.getItem('user'));

    useEffect(() => {
        if (pathname === '/') {
            setIsLibraryVisible(true);
            setIsSettingsVisible(false);

        } else if (pathname === '/settings') {
            setIsLibraryVisible(false);
            setIsSettingsVisible(true);
        }
    }, [])

    const onToggle = () => {
        setToggleMenu(!toggleMenu);
    }

    const onLogout = () => {
        logout(token)
            .then(() => {
                localStorage.removeItem('user');
                navigate('/');
            })
    }

    return (
        <div>
            <div id='hamburger'>
                <div id='hamburger-wrapper'>
                    <div className='hamburger-item'>
                        <img src={toggleMenu ? window.location.origin + '/images/close.png' : window.location.origin + '/images/menu.svg'} alt='Menu' onClick={onToggle} />
                    </div>
                    <div className='hamburger-item'>
                        <img src={window.location.origin + '/images/logo.svg'} className='logo' alt='Logo' />
                    </div>
                    <div className='hamburger-item'>
                        <div>
                            <img
                                src={window.location.origin + '/images/profile.svg'} className={toggleMenu ? 'hamburger-profile-hide' : 'hamburger-profile'} alt='Profile' />
                        </div>
                        <div>
                            <img
                                src={window.location.origin + '/images/logout.svg'} className={toggleMenu ? 'hamburger-logout-hide' : 'hamburger-logout'}
                                alt='Logout'
                                onClick={onLogout}
                            />
                        </div>
                    </div>
                </div>
                <div className={toggleMenu ? 'hide-toggle' : 'show-toggle'}>
                    <div className='hamburger-button library'>
                        <Link to='/' className={isLibraryVisible ? 'hamburger-link-bold' : 'hamburger-link'}>LIBRARY</Link>
                        <div className={isLibraryVisible ? 'selected' : 'hidden'}></div>
                    </div>
                    <div className='hamburger-button settings'>
                        <Link to='/settings' className={isSettingsVisible ? 'hamburger-link-bold' : 'hamburger-link'}>SETTINGS</Link>
                        <div className={isSettingsVisible ? 'selected' : 'hidden'}></div>
                    </div>
                    <div className='hamburger-button settings'>
                        <Link to='/' className={isSettingsVisible ? 'hamburger-link-bold' : 'hamburger-link'} onClick={onLogout}>Logout</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Hamburger;