import './Navbar.css';
import { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import Hamburger from '../Hamburger/Hamburger';
import { logout } from '../../services/authService';

const Navbar = () => {
    const [isLibraryVisible, setIsLibraryVisible] = useState(true);
    const [isSettingsVisible, setIsSettingsVisible] = useState(false);
    const { pathname } = useLocation();
    const { token } = JSON.parse(localStorage.getItem('user'));
    const navigate = useNavigate();

    useEffect(() => {
        if (pathname === '/') {
            setIsLibraryVisible(true);
            setIsSettingsVisible(false);

        } else if (pathname === '/settings') {
            setIsLibraryVisible(false);
            setIsSettingsVisible(true);
        }
    }, [])

    const HomeLocation = (
        <img src={window.location.origin + '/images/logo.svg'} className='logo' alt='Logo' />
    );

    const DetailsLocation = (
        <div id='settings-back-button'>
            <div>
                <img src={window.location.origin + '/images/polygon.png'} alt='Arrow' />
            </div>
            <div>
                <Link to='/'>Library</Link>
            </div>
        </div>
    );

    const onLogout = () => {
        logout(token)
            .then(() => {
                localStorage.removeItem('user');
                navigate('/');
            })
    }

    return (
        <>
            <div id='navbar'>
                <div className='navbar-item'>
                    {
                        pathname !== '/settings' && pathname !== '/'
                            ? DetailsLocation
                            : HomeLocation
                    }
                </div>
                <div className='navbar-item'>
                    <div className='navbar-buttons'>
                        <Link to='/' className={isLibraryVisible ? 'hamburger-link-bold' : 'hamburger-link'}>LIBRARY</Link>
                        <div className={isLibraryVisible ? 'selected' : 'hidden'}></div>
                    </div>
                    <div className='navbar-buttons'>
                        <Link to='/settings' className={isSettingsVisible ? 'hamburger-link-bold' : 'hamburger-link'}>SETTINGS</Link>
                        <div className={isSettingsVisible ? 'selected' : 'hidden'}></div>
                    </div>
                </div>
                <div className='navbar-item'>
                    <div>
                        <img
                            src={window.location.origin + '/images/profile.svg'} className='profile' alt='Profile' />
                    </div>
                    <div>
                        <img
                            src={window.location.origin + '/images/logout.svg'} className='profile-logout'
                            alt='Logout'
                            onClick={onLogout}
                        />
                    </div>
                </div>
            </div>
            <Hamburger />
        </>
    )
}

export default Navbar;