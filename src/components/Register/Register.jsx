import './Register.css'
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { register } from '../../services/authService';
import { usernameRegex, passwordRegex } from '../../constants/common'

const Register = () => {
    const navigate = useNavigate();
    const [isVisiblePassword, setIsVisiblePassword] = useState(false);
    const [isVisibleRePassword, setIsVisibleRePassword] = useState(false);
    const [authData, setAuthData] = useState({
        username: '',
        password: '',
        repeatPassword: ''
    });
    const [error, setError] = useState({
        text: '',
        occured: false
    });

    const onVisiblePassword = () => {
        return setIsVisiblePassword(!isVisiblePassword);
    }

    const onVisibleRePassword = () => {
        return setIsVisibleRePassword(!isVisibleRePassword);
    }

    const saveUserData = async (e) => {
        e.preventDefault();

        if (authData.username === '' ||
            authData.password === '' ||
            authData.repeatPassword === '') {
            setError({ ...error, text: 'Please fill the inputs', occured: true });
            return navigate('/register');
        }

        if (authData.password !== authData.repeatPassword) {
            setError({ ...error, text: `Passwords don't match`, occured: true });
            return navigate('/register');
        }

        if (authData.username.length < 4 && usernameRegex.test(authData.username)) {
            setError({ ...error, text: 'Username is too short and contains special symbols(except _)', occured: true });
            return navigate('/register');
        }

        if (authData.password.length < 6 && passwordRegex.test(authData.password)) {
            setError({ ...error, text: 'Password is too short and contains special symbols', occured: true });
            return navigate('/register');
        }

        const userData = await register(authData);

        if (userData.error) {
            setError({ ...error, text: userData.error, occured: true });
            return navigate('/register');
        }

        return navigate('/login');
    }

    return (
        <div id='register-wrapper'>
            <div id='register-container'>
                <div className='logo-container'>
                    <img src={window.location.origin + '/images/logo.svg'} alt='Logo' />
                </div>
                <div id='register-form'>
                    <div>
                        <h2>WELCOME TO THE BEST BOOK DATABASE!</h2>
                        <h4>CREATE YOUR PROFILE</h4>
                    </div>
                    <form>
                        <div className='input-container'>
                            <div>
                                <label htmlFor='username'>Username</label>
                            </div>
                            <div>
                                <input
                                    type='text'
                                    name='username'
                                    className='input'
                                    autoComplete='on'
                                    onChange={(e) => setAuthData({ ...authData, username: e.target.value })}
                                />
                            </div>
                        </div>
                        <div className='input-container'>
                            <div>
                                <label htmlFor='password'>Password</label>
                            </div>
                            <div>
                                <div className='icon-container'>
                                    <i onClick={onVisiblePassword}>
                                        <img
                                            src={window.location.origin + '/images/view.svg'}
                                            alt='View'
                                            className={isVisiblePassword ? 'hide-password' : 'view-password'}
                                        />
                                    </i>
                                </div>
                                <input
                                    type={isVisiblePassword ? 'text' : 'password'}
                                    name='password'
                                    className='input'
                                    autoComplete='on'
                                    onChange={(e) => setAuthData({ ...authData, password: e.target.value })}
                                />
                            </div>
                        </div>
                        <div className='input-container'>
                            <div>
                                <label htmlFor='re-password'>Repeat Password</label>
                            </div>
                            <div>
                                <div className='icon-container'>
                                    <i onClick={onVisibleRePassword}>
                                        <img
                                            src={window.location.origin + '/images/view.svg'}
                                            alt='View'
                                            className={isVisibleRePassword ? 'hide-password' : 'view-password'}
                                        />
                                    </i>
                                </div>
                                <input
                                    type={isVisibleRePassword ? 'text' : 'password'}
                                    name='re-password'
                                    className='input'
                                    autoComplete='on'
                                    onChange={(e) => setAuthData({ ...authData, repeatPassword: e.target.value })}
                                />
                            </div>
                        </div>
                        <div class={error.occured ? 'error-visible' : 'error-hidden'}>
                            <div>
                                <img src={window.location.origin + '/images/close.png'} alt="Close" />
                            </div>
                            <div class='error-span-container'>
                                <span>{error.text}</span>
                            </div>
                        </div>
                        <div className='button-container'>
                            <button onClick={(e) => saveUserData(e)}>
                                SIGN UP
                            </button>
                        </div>
                    </form>
                    <div className='bottom-container'>
                        <div>You have an account?</div>
                        <div>
                            <Link to='/login'>
                                LOG IN HERE
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className='image-wrapper'>
                <img src={window.location.origin + '/images/book.png'} alt='Book' />
            </div>
        </div>
    )
}

export default Register;
