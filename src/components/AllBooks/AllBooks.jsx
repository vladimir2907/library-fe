import './AllBooks.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { getAllBooks, searchBooks } from '../../services/bookService';

import Book from '../Book/Book';

const AllBooks = () => {
    const [allBooks, setAllBooks] = useState([]);
    const [query, setQuery] = useState('');
    const navigate = useNavigate();
    const { token } = JSON.parse(localStorage.getItem('user'));

    useEffect(() => {
        const { token } = JSON.parse(localStorage.getItem('user'));

        getAllBooks(token)
            .then(data => {
                setAllBooks(data);
                navigate('/');
            })

    }, []);

    const onSearch = (e) => {
        e.preventDefault();
        if (query) {
            searchBooks(token, query)
                .then(data => {
                    setAllBooks(data);
                    navigate('/');
                })
        } else {
            return navigate('/');
        }
    }

    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            if (query) {
                searchBooks(token, query)
                    .then(data => {
                        setAllBooks(data);
                        navigate('/');
                    })
            } else {
                return navigate('/');
            }
        }
    }

    return (
        <div>
            <div className='search-container'>
                <div className='all-books'>
                    <h3>ALL BOOKS</h3>
                </div>
                <div className='search-input-wrapper'>
                    <div className='search-icon-container'>
                        <i onClick={onSearch}>
                            <img src={window.location.origin + '/images/search.png'} className='search' alt='Search' />
                        </i>
                    </div>
                    <div>
                        <input
                            name='password'
                            type='text'
                            className='search-input'
                            placeholder='Search'
                            onKeyDown={(e) => handleKeyDown(e)}
                            onChange={(e) => setQuery(e.target.value)}
                        />
                    </div>
                </div>
            </div>
            <div id='all-books'>
                {allBooks.map(book => (
                    <Book key={book._id} book={book} />
                ))}
            </div>
        </div>

    )
}

export default AllBooks;