import './Login.css'
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { login } from '../../services/authService'
import { localStorageUser, usernameRegex, passwordRegex } from '../../constants/common'

const Login = () => {
    const navigate = useNavigate();
    const [isVisible, setisVisible] = useState(false);
    const [authData, setAuthData] = useState({
        username: '',
        password: '',
    });
    const [error, setError] = useState({
        text: '',
        occured: false
    });

    const onVisible = () => {
        return setisVisible(!isVisible);
    }

    const saveUserData = async (e) => {
        e.preventDefault();

        if (authData.username === '' ||
            authData.password === '') {
            setError({ ...error, text: 'Please fill the inputs', occured: true });
            return navigate('/login');
        }

        if (authData.username.length < 4 && usernameRegex.test(authData.username)) {
            setError({ ...error, text: 'Username is too short and contains special symbols(except _)', occured: true });
            return navigate('/login');
        }

        if (authData.password.length < 6 && passwordRegex.test(authData.password)) {
            setError({ ...error, text: 'Password is too short and contains special symbols', occured: true });
            return navigate('/login');
        }

        const userData = await login(authData);

        if (userData.error) {
            setError({ ...error, text: userData.error, occured: true });
            return navigate('/login');
        }

        localStorage.setItem(localStorageUser, JSON.stringify(userData));

        return navigate('/');
    }

    return (
        <div id='login-wrapper'>
            <div id='login-container'>
                <div className='logo-container'>
                    <img src={window.location.origin + '/images/logo.svg'} alt='Logo' />
                </div>
                <div id='login-form'>
                    <h2>WELCOME BACK!</h2>
                    <form>
                        <div className='input-container'>
                            <div>
                                <label htmlFor='email'>Username</label>
                            </div>
                            <div>
                                <input
                                    type='text'
                                    name='username'
                                    className='input'
                                    autoComplete='on'
                                    onChange={(e) => setAuthData({ ...authData, username: e.target.value })}
                                />
                            </div>
                        </div>
                        <div className='input-container'>
                            <div>
                                <label htmlFor='password'>Password</label>
                            </div>
                            <div>
                                <div className='icon-container'>
                                    <i onClick={onVisible}>
                                        <img
                                            src={window.location.origin + '/images/view.svg'}
                                            alt='View'
                                            className={isVisible ? 'hide-password' : 'view-password'}
                                        />
                                    </i>
                                </div>
                                <input
                                    type={isVisible ? 'text' : 'password'}
                                    name='password'
                                    className='input'
                                    autoComplete='on'
                                    onChange={(e) => setAuthData({ ...authData, password: e.target.value })}
                                />
                            </div>
                        </div>
                        <div id='recover-password'>
                            <Link to='/'>
                                Recover password
                            </Link>
                        </div>
                        <div className={error.occured ? 'error-visible' : 'error-hidden'}>
                            <div>
                                <img src={window.location.origin + '/images/close.png'} alt="Close" />
                            </div>
                            <div className='error-span-container'>
                                <span>{error.text}</span>
                            </div>
                        </div>
                        <div className='button-container'>
                            <button onClick={(e) => saveUserData(e)}>
                                LOG IN
                            </button>
                        </div>
                    </form>
                    <div className='bottom-container'>
                        <div>You don't have an account?</div>
                        <div>
                            <Link to='/register'>
                                SIGN UP HERE
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className='image-wrapper'>
                <img src={window.location.origin + '/images/book.png'} alt='Book' />
            </div>
        </div>
    )
}

export default Login;
