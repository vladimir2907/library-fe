import './Details.css';
import { useParams, Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

import Navbar from '../Navbar/Navbar';
import { getOneBook } from '../../services/bookService';

const Details = () => {
    const { bookId } = useParams();
    const [currentBook, setCurrentBook] = useState([]);
    const { token } = JSON.parse(localStorage.getItem('user'));

    useEffect(() => {
        getOneBook(bookId, token)
            .then(data => {
                let [createdOn] = data.createOn.split('T');
                data.createOn = createdOn.split('-').reverse().join('.');

                let [updatedOn] = data.lastUpdateOn.split('T');
                data.lastUpdateOn = updatedOn.split('-').reverse().join('.');

                setCurrentBook([data]);
            })
    }, []);

    return (
        <div>
            <Navbar />
            <div id='back'>
                <div>
                    <img src={window.location.origin + '/images/polygon.png'} />
                </div>
                <div>
                    <Link to='/'>Library</Link>
                </div>
            </div>
            {
                currentBook.map((book) => (
                    <div key={book._id} id='book-details'>
                        <div id='book-image-container'>
                            <img src={book.image} alt="" />
                        </div>
                        <div id='details-wrapper'>
                            <div id='book-title'>
                                <h2>{book.name}</h2>
                            </div>
                            <div id="details-information">
                                <div id='book-author'>
                                    <p>{book.author}</p>
                                </div>
                                <div>
                                    <p>
                                        Genre: <b>{book.genre.name}</b>
                                    </p>
                                </div>
                                <div>
                                    <p>Created on: <b>{book.createOn}</b></p>
                                </div>
                                <div>
                                    <p>Updated on: <b>{book.lastUpdateOn}</b></p>
                                </div>
                                <div id="book-description">
                                    <div>
                                        <p><b>Short description</b></p>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                        Suscipit voluptate itaque perspiciatis tempora deserunt,
                                        natus culpa sunt autem exercitationem ipsum quidem dignissimos
                                        voluptatem dolore nostrum dolorum ullam, consequuntur numquam
                                        dolorem dicta minima nisi fugit, accusantium commodi? Aliquid
                                        quam dignissimos mollitia?
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default Details;