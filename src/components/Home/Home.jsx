import './Home.css'
import Navbar from "../Navbar/Navbar";
import AllBooks from '../AllBooks/AllBooks';

const Home = () => {
    return (
        <>
            <Navbar />
            <div id='home-wrapper'>
                <AllBooks />
            </div>
        </>
    )
}

export default Home;