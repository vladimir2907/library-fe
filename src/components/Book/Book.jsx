import './Book.css';
import { Link } from 'react-router-dom';

const Book = ({ book }) => {
    let [createdOn] = book.createOn.split('T');
    createdOn = createdOn.split('-').reverse().join('.');
    let [updatedOn] = book.lastUpdateOn.split('T');
    updatedOn = updatedOn.split('-').reverse().join('.');

    return (
        <div key={book._id} className='book'>
            <div className='book-image'>
                <img src={book.image} alt='' />
            </div>
            <div className='book-information'>
                <div>
                    <h3>{book.name}</h3>
                </div>
                <div className='book-author'>
                    <p>{book.author}</p>
                </div>
                <div className='book-genre'>
                    <p>Genre: <b>{book.genre.name}</b></p>
                </div>
                <div className='date-container'>
                    <div>
                        <p>Created on: <b>{createdOn}</b></p>
                    </div>
                    <div>
                        <p>Updated on: <b>{updatedOn}</b></p>
                    </div>
                </div>
            </div>
            <div className='arrow-link'>
                <div className='arrow'>
                    <Link to={`/book/${book._id}`}>
                        <img src={window.location.origin + '/images/arrow.svg'} alt='Arrow' />
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default Book;