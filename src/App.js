import './App.css';
import { useRoutes } from 'react-router-dom';
import routeConfig from './routes';

function App() {
  const isLoggedIn = Boolean(JSON.parse(localStorage.getItem('user')));

  const routing = useRoutes(routeConfig(isLoggedIn));
  return (
    <div className='app'>
      {routing}
    </div>
  );
}

export default App;
