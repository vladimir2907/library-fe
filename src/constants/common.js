export const localStorageUser = 'user';
export const usernameRegex = /^[A-Za-z0-9_]+$/;
export const passwordRegex = /^[A-Za-z0-9]+$/;
export const url = 'https://books-library-dev.herokuapp.com'